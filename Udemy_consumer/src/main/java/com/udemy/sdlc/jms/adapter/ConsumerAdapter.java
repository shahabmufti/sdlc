package com.udemy.sdlc.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());

	private static final String MONGODB_VENDOR = "vendor";
	
	public ConsumerAdapter() {
		super();
	}
	
	public void sendToMongo(String json) throws UnknownHostException {
		logger.info("Sending to MongoDB");
		MongoClient client = new MongoClient();
		DB db = client.getDB(MONGODB_VENDOR);
		DBCollection collection = db.getCollection("contact");
		logger.info("Converting JSON to DBObject");
		DBObject object = (DBObject) JSON.parse(json);
		collection.insert(object);
		logger.info("Sent to MongoDB");
		
/*		DBCursor cursorDoc = collection.find();
		while (cursorDoc.hasNext()) {
			System.out.println(cursorDoc.next());
		}

		logger.info("Done!!");*/
		
	}

	public void retrieveFromMongo() throws UnknownHostException {
		logger.info("Retrieve from MongoDB");
		MongoClient client = new MongoClient();
		DB db = client.getDB(MONGODB_VENDOR);
		DBCollection collection = db.getCollection("contact");
		DBCursor cursorDoc = collection.find();
		while (cursorDoc.hasNext()) {
			logger.info(cursorDoc.next());
		}

		logger.info("Done!!");
	}
}
